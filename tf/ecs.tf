resource "aws_security_group" "ecs-demo-sg-createdby-tf" {
  name        = "ecs-demo-sg-createdby-tf"
  description = "ecs security group for demo"
  vpc_id      = "vpc-0255b3d102925e1e4"

  ingress {
    description      = "TLS from VPC"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "demo-ecs"
  }
}

resource "aws_cloudwatch_log_group" "ecs-demo-log-group" {
  name = "ecs-log-group"

  tags = {
    Environment = "demo"
    Application = "demo-app"
  }
}

resource "aws_ecs_cluster" "demo-ecs-cluster" {
  name = "ecs-cluster-for-demo"
}

resource "aws_ecs_service" "demo-ecs-service-two" {
  name            = "demo-app"
  cluster         = aws_ecs_cluster.demo-ecs-cluster.id
  task_definition = aws_ecs_task_definition.demo-ecs-task-definition.arn
  launch_type     = "FARGATE"
  network_configuration {
#    security_groups = ["sg-02b55bfaecd28a054"]
    security_groups = [resource.aws_security_group.ecs-demo-sg-createdby-tf.id]
    subnets          = ["subnet-013d6160378fdf95e"]
    assign_public_ip = true
  }
  desired_count = 1
}

resource "aws_ecs_task_definition" "demo-ecs-task-definition" {
  family                   = "ecs-task-definition-demo"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  memory                   = "512"
  cpu                      = "256"
  execution_role_arn       = "arn:aws:iam::014675943777:role/ecsTaskExecutionRole"
  container_definitions    = <<EOF
[
  {
    "name": "demo-container",
    "image": "014675943777.dkr.ecr.us-east-1.amazonaws.com/demo-repo:latest",
    "cpu": 0,
    "essential": true,
    "logConfiguration": {
        "logDriver": "awslogs",
        "secretOptions": null,
        "options": {
          "awslogs-group": "ecs-log-group",
          "awslogs-region": "us-east-1",
          "awslogs-stream-prefix": "ecs"
        }
    },    
    "portMappings": [
      {
        "containerPort": 8080,
        "hostPort": 8080
      }
    ]
  }
]
EOF
}